package br.com.atlasco.fragments;

import br.com.atlasco.R;
import br.com.atlasco.db.actions.DoUpdateJob;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.utils.DBException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SetTitleFragment extends DialogFragment {

	// Current job
	private final Job mJob;

	/**
	 * Initialize job instance
	 * 
	 * @param job
	 */
	public SetTitleFragment(Job job) {
		mJob = job;
	}

	/*
	 * The activity that creates an instance of this dialog fragment must
	 * implement this interface in order to receive event callbacks. Each method
	 * passes the DialogFragment in case the host needs to query it.
	 */
	public interface NoticeDialogListener {
		public void onDialogPositiveClick(DialogFragment dialog);
	}

	// Use this instance of the interface to deliver action events
	NoticeDialogListener mListener;

	// Override the Fragment.onAttach() method to instantiate the
	// NoticeDialogListener
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// Verify that the host activity implements the callback interface
		try {
			// Instantiate the NoticeDialogListener so we can send events to the
			// host
			mListener = (NoticeDialogListener) activity;
		} catch (ClassCastException e) {
			// The activity doesn't implement the interface, throw exception
			throw new ClassCastException(activity.toString() + " must implement NoticeDialogListener");
		}
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		final View dialogView = inflater.inflate(R.layout.dialog_edit_job, null);

		builder.setView(dialogView)
		// Add action buttons
				.setPositiveButton(R.string.dialog_positive_button, null).setNegativeButton(R.string.dialog_negative_button, null);

		final AlertDialog alertDialog = builder.create();

		// Set this listener to set other listener for add task button
		alertDialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				
				EditText oldDesription = (EditText) dialogView.findViewById(R.id.job_description);
				oldDesription.setText(mJob.getDescription());

				Button submit = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
				// Sets the behavior when positive button is clicked
				submit.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {

						try {

							EditText newDescription = (EditText) dialogView.findViewById(R.id.job_description);
							
							// new value
							mJob.setDescription(newDescription.getText().toString());

							// Update job description with a new value
							DoUpdateJob duj = new DoUpdateJob(getActivity().getApplicationContext(), mJob);
							duj.run();

							Toast.makeText(getActivity(), R.string.message_description_changed, Toast.LENGTH_SHORT).show();

							// Host must refresh
							mListener.onDialogPositiveClick(SetTitleFragment.this);
						} catch (DBException e) {
							Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
						}
					}
				});

			}
		});

		return alertDialog;
	}
}
