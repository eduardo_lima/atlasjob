package br.com.atlasco;
import br.com.atlasco.R;
import br.com.atlasco.db.actions.DoNewAction;
import br.com.atlasco.db.utils.DBException;
import android.app.Application;


public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		DoNewAction action0;
		try {
			action0 = new DoNewAction(getApplicationContext(), getString(R.string.button_go_customer));
			action0.run();
			action0 = new DoNewAction(getApplicationContext(), getString(R.string.button_start_service));
			action0.run();
			action0 = new DoNewAction(getApplicationContext(), getString(R.string.button_end_service));
			action0.run();
			action0 = new DoNewAction(getApplicationContext(), getString(R.string.button_go_home));
			action0.run();
		} catch (DBException e) {
			e.printStackTrace();
		}

	}
}
