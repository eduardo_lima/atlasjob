package br.com.atlasco.activities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import br.com.atlasco.R;
import br.com.atlasco.db.actions.DoGetAllJob;
import br.com.atlasco.db.actions.DoGetJob;
import br.com.atlasco.db.actions.DoGetMarking;
import br.com.atlasco.db.actions.DoGetTrack;
import br.com.atlasco.db.actions.DoNewLocation;
import br.com.atlasco.db.actions.DoNewJob;
import br.com.atlasco.db.actions.DoNewMarking;
import br.com.atlasco.db.actions.DoNewTrack;
import br.com.atlasco.db.actions.DoUpdateJob;
import br.com.atlasco.db.utils.DBException;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.fragments.SetTitleFragment;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MainActivity extends ListActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener,
		SetTitleFragment.NoticeDialogListener {

	private static final String TAG = MainActivity.class.getSimpleName();

	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private static int DISPLACEMENT = 10; // 10 meters

	private static int FATEST_INTERVAL = 5000; // 5 sec

	private static int UPDATE_INTERVAL = 10000; // 10 sec
	
	public final static String VALUE_JOB_ID = "br.com.atlasco.activities.JOB_ID";

	private SimpleAdapter mAdapter;

	private Job mCurrentJob;

	private Track mCurrentTrack;

	private List<HashMap<String, String>> mList;

	private DialogFragment mDialog;

	private LocationRequest mLocationRequest;

	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	// boolean flag to toggle periodic location updates
	private boolean mRequestingLocationUpdates = false;

	/**
	 * Creating google api client object
	 * */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();

	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_SHORT).show();
				// finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Creating location request object
	 **/
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 10 meters
	}

	/**
	 * Start a new job and recording time and distance elapsed
	 * 
	 * @param v
	 *            Widget which performed this action
	 */
	public void goCustomer(View v) {

		try {
			if (mCurrentJob != null) {
				throw new IllegalArgumentException(getString(R.string.message_has_job_running));
			}
			if (mGoogleApiClient == null) {
				throw new IllegalArgumentException(getString(R.string.message_null_google_api));
			}

			// Create a new job with date format as a title
			DoNewJob dnj = new DoNewJob(getApplicationContext(), new DateTime(new Date()).toString(DateTimeFormat
					.forPattern("EEE, MMM d, ''yy")));
			dnj.run();

			// Get job created and set as the current job
			DoGetJob dgj = new DoGetJob(getApplicationContext(), dnj.getmGeneratedId());
			dgj.run();
			mCurrentJob = dgj.getmJob(); // Set current job

			// Create the first marking of the job
			DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 1);
			dnm.run();

			// Create the correspondent track of the job
			DoNewTrack dnt = new DoNewTrack(getApplicationContext(), mCurrentJob.getId());
			dnt.run();

			// Get generated track to start getting coordinates
			DoGetTrack dgt = new DoGetTrack(getApplicationContext(), dnt.getmGeneratedId());
			dgt.run();
			mCurrentTrack = dgt.getmTrack();

			Toast.makeText(getApplicationContext(), getResources().getString(R.string.service_started), Toast.LENGTH_SHORT).show();

			this.refreshJobList();

			// After all, it should get the start to get the location
			this.togglePeriodicLocationUpdates();

		} catch (DBException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		} catch (IllegalArgumentException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Start record a new track for returning way
	 * 
	 * @param v
	 */
	public void goEndService(View v) {

		try {
			// Test if currentJob has a value
			if (mCurrentJob == null) {
				throw new IllegalArgumentException(getString(R.string.message_no_job_running));
			}

			// Test if there is two actions from currentJob
			DoGetMarking dgm = new DoGetMarking(getApplicationContext(), mCurrentJob.getId());
			dgm.run();
			if (dgm.getmList().size() != 2) {
				throw new IllegalArgumentException(getString(R.string.message_action_not_allowed));
			}

			// create a new marking with action 3
			DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 3);
			dnm.run();

			// update job status
			mCurrentJob.setStatus(getString(R.string.status_in_transit));
			DoUpdateJob duj = new DoUpdateJob(getApplicationContext(), mCurrentJob);
			duj.run();

			// Create the correspondent track of the job
			DoNewTrack dnt = new DoNewTrack(getApplicationContext(), mCurrentJob.getId());
			dnt.run();

			// Get generated track to start getting coordinates
			DoGetTrack dgt = new DoGetTrack(getApplicationContext(), dnt.getmGeneratedId());
			dgt.run();
			mCurrentTrack = dgt.getmTrack();

			this.refreshJobList();

			// stop location updates
			this.togglePeriodicLocationUpdates();

		} catch (IllegalArgumentException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		} catch (DBException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	public void goFinishJob(View v) {

		try {
			// Test if currentJob has a value
			if (mCurrentJob == null) {
				throw new IllegalArgumentException(getString(R.string.message_no_job_running));
			}

			// Test if there is only one marking from currentJob
			DoGetMarking dgm = new DoGetMarking(getApplicationContext(), mCurrentJob.getId());
			dgm.run();
			if (dgm.getmList().size() != 3) {
				throw new IllegalArgumentException(getString(R.string.message_action_not_allowed));
			}

			// create a new marking with action 4
			DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 4);
			dnm.run();

			// update job status
			mCurrentJob.setStatus(getString(R.string.status_job_done));
			DoUpdateJob duj = new DoUpdateJob(getApplicationContext(), mCurrentJob);
			duj.run();

			// Call for fragment to edit job description
			Job copy = new Job();
			copy.setId(mCurrentJob.getId());
			copy.setDescription(mCurrentJob.getDescription());
			copy.setCreatedAt(mCurrentJob.getCreatedAt());
			copy.setStatus(mCurrentJob.getStatus());
			mDialog = new SetTitleFragment(copy);
			mDialog.show(getFragmentManager(), SetTitleFragment.class.getSimpleName());

			this.refreshJobList();

			// stop location updates
			this.togglePeriodicLocationUpdates();

			// erase current job and track
			mCurrentJob = null;
			mCurrentTrack = null;

		} catch (IllegalArgumentException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		} catch (DBException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}

	}

	/**
	 * Stop recording track, distance and running time
	 * 
	 * @param v
	 */
	public void goStartService(View v) {

		try {
			// Test if currentJob has a value
			if (mCurrentJob == null) {
				throw new IllegalArgumentException(getString(R.string.message_no_job_running));
			}

			// Test if there is only one marking from currentJob
			DoGetMarking dgm = new DoGetMarking(getApplicationContext(), mCurrentJob.getId());
			dgm.run();
			if (dgm.getmList().size() != 1) {
				throw new IllegalArgumentException(getString(R.string.message_action_not_allowed));
			}

			// create a new marking with action 2
			DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 2);
			dnm.run();

			// update job status
			mCurrentJob.setStatus(getString(R.string.status_at_customer));
			DoUpdateJob duj = new DoUpdateJob(getApplicationContext(), mCurrentJob);
			duj.run();

			this.refreshJobList();

			// stop location updates
			this.togglePeriodicLocationUpdates();

		} catch (IllegalArgumentException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		} catch (DBException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + arg0.getErrorCode());
	}

	@Override
	public void onConnected(Bundle arg0) {
		// Once connected with google api, get the location
		// displayLocation();

		if (mRequestingLocationUpdates) {
			startLocationUpdates();
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		if (mGoogleApiClient != null) {
			((GoogleApiClient) mGoogleApiClient).connect();
		}
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {

		mDialog.dismiss();

		this.refreshJobList();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		@SuppressWarnings("unchecked")
		HashMap<String, String> hm = (HashMap<String, String>) l.getItemAtPosition(position);
		
		Intent i = new Intent(MainActivity.this, ShowInfoActivity.class);
		i.putExtra(VALUE_JOB_ID, Integer.parseInt(hm.get("id")));
		
		startActivity(i);
	}

	@Override
	public void onLocationChanged(Location arg0) {

		try {
			// Assign the new location and save it
			DoNewLocation dnc = new DoNewLocation(getApplicationContext(), mCurrentTrack.getId(), arg0.getLatitude(), arg0.getLongitude());
			dnc.run();
		} catch (DBException e) {
			Log.e(TAG, e.getMessage()); // dont show toast kozz it's a loop
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Init list
		mList = new ArrayList<HashMap<String, String>>();

		// Keys used in Hashmap
		String[] from = { "id", "description", "sub_description", "created_at" };

		// Ids of views in listview_layout
		int[] to = { R.id.id, R.id.description, R.id.sub_description, R.id.created_at };

		// Instantiating an adapter to store each items
		// R.layout.listview_layout defines the layout of each item
		mAdapter = new SimpleAdapter(getApplicationContext(), mList, R.layout.item_list_jobs, from, to);

		setListAdapter(mAdapter);

		refreshJobList();

		// First we need to check availability of play services
		if (checkPlayServices()) {

			// Building the GoogleApi client
			buildGoogleApiClient();

			createLocationRequest();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (((mGoogleApiClient != null) && ((GoogleApiClient) mGoogleApiClient).isConnected()) && mRequestingLocationUpdates) {
			stopLocationUpdates();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Resuming the periodic location updates
		if (((mGoogleApiClient != null) && ((GoogleApiClient) mGoogleApiClient).isConnected()) && mRequestingLocationUpdates) {
			startLocationUpdates();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	/**
	 * Refresh list of jobs
	 */
	private void refreshJobList() {
		try {
			mList.clear();

			// DB call
			DoGetAllJob dgaj = new DoGetAllJob(getApplicationContext());

			dgaj.run();

			List<Job> jobs = dgaj.getmList();

			for (Job j : jobs) {
				HashMap<String, String> hm = new HashMap<String, String>();
				hm.put("id", Integer.toString(j.getId()));
				hm.put("description", j.getDescription());
				hm.put("sub_description", j.getStatus());
				hm.put("created_at", new DateTime(j.getCreatedAt()).toString(DateTimeFormat.forPattern("dd 'de' MMMM 'de' yyyy")));
				mList.add(hm);
			}

			mAdapter.notifyDataSetChanged();

		} catch (DBException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}

	}

	/**
	 * Starting the location updates
	 * */
	protected void startLocationUpdates() {
		LocationServices.FusedLocationApi.requestLocationUpdates((GoogleApiClient) mGoogleApiClient, mLocationRequest, this);
	}

	/**
	 * Stopping location updates
	 */
	protected void stopLocationUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates((GoogleApiClient) mGoogleApiClient, this);
	}

	/**
	 * Method to toggle periodic location updates
	 * */
	private void togglePeriodicLocationUpdates() {
		if (!mRequestingLocationUpdates) {

			mRequestingLocationUpdates = true;

			// Starting the location updates
			startLocationUpdates();

			Log.d(TAG, "Periodic location updates started!");

		} else {

			mRequestingLocationUpdates = false;

			// Stopping the location updates
			stopLocationUpdates();

			Log.d(TAG, "Periodic location updates stopped!");
		}
	}
}
