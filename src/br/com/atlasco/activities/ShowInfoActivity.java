package br.com.atlasco.activities;

import java.util.List;

import br.com.atlasco.R;
import br.com.atlasco.db.actions.DoGetAllLocation;
import br.com.atlasco.db.actions.DoGetAllTrack;
import br.com.atlasco.db.actions.DoGetJob;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.model.Location;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.db.utils.DBException;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class ShowInfoActivity extends Activity {

	private static final String TAG = ShowInfoActivity.class.getSimpleName();

	private Job mSelectedJob;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_info);

		// Get intent value
		int id = getIntent().getIntExtra(MainActivity.VALUE_JOB_ID, 0);

		try {
			// Get Job instance
			DoGetJob dgj = new DoGetJob(getApplicationContext(), id);
			dgj.run();
			mSelectedJob = dgj.getmJob();

			// Show job description on actionbar view
			getActionBar().setTitle(mSelectedJob.getDescription());

			// Get tracks of the job
			DoGetAllTrack dgat = new DoGetAllTrack(getApplicationContext(), mSelectedJob.getId());
			dgat.run();
			List<Track> tracks = dgat.getmList();

			// Calculate distance
			float sumedDistance = 0f;
			for (Track t : tracks) {
				DoGetAllLocation dgal = new DoGetAllLocation(getApplicationContext(), t.getId());
				dgal.run();
				List<Location> locations = dgal.getmList(); // locatiosn
															// retrieved

				if (locations.size() > 0) { // secure

					Location previous = locations.get(0);

					for (Location l : locations) {
						android.location.Location x = new android.location.Location("point x");
						x.setLatitude(previous.getLatitude());
						x.setLongitude(previous.getLongitude());

						android.location.Location y = new android.location.Location("point y");
						y.setLatitude(l.getLatitude());
						y.setLongitude(l.getLongitude());

						// in meters
						sumedDistance += x.distanceTo(y);

						previous = l;
					}

					Log.d(TAG, sumedDistance + " meters");
				}
			}

			// Set it on the view
			TextView distanceTraveled = (TextView) findViewById(R.id.distance_traveled);
			distanceTraveled.setText("Metros percorridos:" + Double.toString(sumedDistance));

		} catch (DBException e) {
			Toast.makeText(getApplicationContext(), R.string.message_problems_when_loading_job, Toast.LENGTH_SHORT).show();
		}

	}
}
