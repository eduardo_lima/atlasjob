package br.com.atlasco.db.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Contains methods to help execute DDL commands and also has subclasses to
 * execute DML commands
 * 
 * @author eduardo-lima
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	// To identify log lines in the logcat
	private static final String TAG = "DatabaseHelper";

	// Version of the database
	private static final int DATABASE_VERSION = 1;

	// Name of the database
	private static final String DATABASE_NAME = "atlasco";

	// DDL commands
	private static final String CREATE_TABLE_JOBS = "CREATE TABLE " + Consts.TABLE_JOBS + "(" + Consts.KEY_ID + " INTEGER PRIMARY KEY, "
			+ Consts.KEY_DESCRIPTION + " TEXT, " + Consts.KEY_CREATED_AT + " DATE, " + Consts.KEY_STATUS + " TEXT)";

	private static final String CREATE_TABLE_ACTIONS = "CREATE TABLE " + Consts.TABLE_ACTIONS + "(" + Consts.KEY_ID
			+ " INTEGER PRIMARY KEY, " + Consts.KEY_DESCRIPTION + " TEXT)";

	private static final String CREATE_TABLE_MARKINGS = "CREATE TABLE " + Consts.TABLE_MARKINGS + "(" + Consts.KEY_ID
			+ " INTEGER PRIMARY KEY, " + Consts.KEY_JOB_ID + " INTEGER, " + Consts.KEY_ACTION_ID + " INTEGER, " + Consts.KEY_CREATED_AT
			+ " DATE, FOREIGN KEY(" + Consts.KEY_JOB_ID + ") REFERENCES " + Consts.TABLE_JOBS + "(" + Consts.KEY_ID + "), FOREIGN KEY("
			+ Consts.KEY_ACTION_ID + ") REFERENCES " + Consts.TABLE_JOBS + "(" + Consts.KEY_ID + "))";

	public static final String CREATE_TABLE_TRACKS = "CREATE TABLE " + Consts.TABLE_TRACKS + "(" + Consts.KEY_ID
			+ " INTEGER PRIMARY KEY, " + Consts.KEY_JOB_ID + " INTEGER, " + Consts.KEY_DISTANCE_TRAVELED + " REAL,"
			+ " FOREIGN KEY(" + Consts.KEY_JOB_ID + ") REFERENCES " + Consts.TABLE_JOBS + "(" + Consts.KEY_ID + "))";
	
	public static final String CREATE_TABLE_COORDINATES = "CREATE TABLE " + Consts.TABLE_LOCATIONS + "(" + Consts.KEY_ID
			+ " INTEGER PRIMARY KEY, " + Consts.KEY_TRACK_ID + " INTEGER, " + Consts.KEY_LATITUDE + " REAL, " + Consts.KEY_LONGITUDE 
			+ " REAL, FOREIGN KEY(" + Consts.KEY_TRACK_ID + ") REFERENCES " + Consts.TABLE_TRACKS + "(" + Consts.KEY_ID + "))";
	
	private static DatabaseHelper instance;

	/**
	 * Get an unique instance to manipulate data
	 * 
	 * @param context
	 *            Context of the application
	 * @return instance for database interaction
	 */
	public static DatabaseHelper getInstance(Context context) {
		// Singleton
		if (instance == null) {
			instance = new DatabaseHelper(context.getApplicationContext());
		}
		return instance;
	}

	/**
	 * Constructor sets the context
	 * 
	 * @param context
	 *            Context of the application
	 */
	private DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i(TAG, "Creating tables");

		db.execSQL(CREATE_TABLE_JOBS);
		db.execSQL(CREATE_TABLE_ACTIONS);
		db.execSQL(CREATE_TABLE_MARKINGS);
		db.execSQL(CREATE_TABLE_TRACKS);
		db.execSQL(CREATE_TABLE_COORDINATES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i(TAG, "Upgrading tables from version" + oldVersion + " to " + newVersion);

		db.execSQL("DROP TABLE IF EXISTS " + Consts.TABLE_JOBS);
		db.execSQL("DROP TABLE IF EXISTS " + Consts.TABLE_ACTIONS);
		db.execSQL("DROP TABLE IF EXISTS " + Consts.TABLE_MARKINGS);
		db.execSQL("DROP TABLE IF EXISTS " + Consts.TABLE_TRACKS);
		db.execSQL("DROP TABLE IF EXISTS " + Consts.TABLE_LOCATIONS);
		
		onCreate(db);
	}

}
