package br.com.atlasco.db.utils;

/**
 * Holds all consts for name of eache table column
 * @author eduardo-lima
 *
 */
public class Consts {
	
	// Names of tables
	public static final String TABLE_JOBS = "jobs";
	public static final String TABLE_MARKINGS = "markings";
	public static final String TABLE_ACTIONS = "actions";
	public static final String TABLE_TRACKS = "tracks";
	public static final String TABLE_LOCATIONS = "locations";
	
	// Columns columns
	public static final String KEY_ID = "_id";
	public static final String KEY_DESCRIPTION = "description";
	public static final String KEY_CREATED_AT = "created_at";
	public static final String KEY_STATUS = "status";
	public static final String KEY_ACTION_ID = "action_id";
	public static final String KEY_JOB_ID = "job_id";
	public static final String KEY_TRACK_ID = "track_id";
	public static final String KEY_DISTANCE_TRAVELED = "distance_traveled";
	public static final String KEY_LATITUDE = "latitude";
	public static final String KEY_LONGITUDE = "longitude";
}
