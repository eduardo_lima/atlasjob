package br.com.atlasco.db.actions;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.db.utils.Consts;

/**
 * Get specifc track from db
 * 
 * @author eduardo-lima
 *
 */
public class DoGetTrack extends ActionBase {

	private static final String TAG = DoGetTrack.class.getSimpleName();

	private long mTrackID;

	private Track mTrack;

	/**
	 * Assign the job id
	 * 
	 * @param c
	 *            Context of the application
	 * @param id
	 *            Job id
	 */
	public DoGetTrack(Context c, long id) {
		super(c);

		Log.i(TAG, "Initializing " + DoGetTrack.class.getSimpleName());

		mTrackID = id;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Getting track...");

		String selectQuery = "SELECT " + Consts.TABLE_TRACKS + "." + Consts.KEY_ID + " C1, " + Consts.TABLE_TRACKS + "."
				+ Consts.KEY_JOB_ID + " C2, " + Consts.TABLE_TRACKS + "." + Consts.KEY_DISTANCE_TRAVELED + " C3 FROM "
				+ Consts.TABLE_TRACKS + " WHERE C1 = " + mTrackID;

		Cursor cursor = this.getDatabase().rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			mTrack = new Track();
			mTrack.setId(cursor.getLong(cursor.getColumnIndex("C1")));
			mTrack.setJobId(cursor.getLong(cursor.getColumnIndex("C2")));
			mTrack.setDistanceTraveled(cursor.getFloat(cursor.getColumnIndex("C3")));
		}
	}

	public Track getmTrack() {
		return mTrack;
	}
}
