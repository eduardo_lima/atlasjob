package br.com.atlasco.db.actions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.atlasco.db.utils.Consts;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

/**
 * Action to create new markings
 * 
 * @author eduardo-lima
 *
 */
public class DoNewMarking extends ActionBase {

	private static final String TAG = "DoNewMarking";

	private final long mJobID;
	private final long mActionID;

	/**
	 * Initialize with context and the values for a new job
	 * 
	 * @param c
	 *            Context of the application
	 * @param jobID
	 *            Each marking has a job related
	 */
	public DoNewMarking(Context c, long jobID, long actionID) {
		super(c);
		
		Log.i(TAG, "Initializing DoNewMarking");

		mJobID = jobID;
		mActionID = actionID;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Creating marking...");

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

		ContentValues values = new ContentValues();
		values.put(Consts.KEY_ACTION_ID, mActionID);
		values.put(Consts.KEY_JOB_ID, mJobID);
		values.put(Consts.KEY_CREATED_AT, dateFormat.format(new Date()));
		this.getDatabase().insert(Consts.TABLE_MARKINGS, null, values);
	}

}
