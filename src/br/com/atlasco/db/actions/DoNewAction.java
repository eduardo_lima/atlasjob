package br.com.atlasco.db.actions;

import br.com.atlasco.db.utils.Consts;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

/**
 * Action to create new actions
 * 
 * @author eduardo-lima
 *
 */
public class DoNewAction extends ActionBase {

	private static final String TAG = "DoNewAction";

	private final String mDescription;

	/**
	 * Initialize with context and the values for a new action
	 * 
	 * @param c
	 *            Context of the application
	 * @param description
	 *            Description of the task F
	 */
	public DoNewAction(Context c, String d) {
		super(c);

		Log.i(TAG, "Initializing DoNewAction");

		mDescription = d;
	}
	
	@Override
	protected boolean allowExecute() {
		
		String[] where = new String[1];
		where[0] = mDescription;

		Cursor cursor = this.getDatabase().query(Consts.TABLE_ACTIONS, null, Consts.KEY_DESCRIPTION + " LIKE ?", where, null, null,
				null);

		// activity creation only when there is no existing rows with the
		// description
		return cursor.getCount() <= 0;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Creating action " + mDescription);

		ContentValues values = new ContentValues();
		values.put(Consts.KEY_DESCRIPTION, mDescription);
		this.getDatabase().insert(Consts.TABLE_ACTIONS, null, values);
	}

}
