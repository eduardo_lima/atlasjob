package br.com.atlasco.db.actions;

import br.com.atlasco.db.utils.Consts;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

/**
 * Action to create new tracks
 * 
 * @author eduardo-lima
 *
 */
public class DoNewTrack extends ActionBase {

	private static final String TAG = DoNewTrack.class.getSimpleName();

	private final long mJobID;
	private long mGeneratedId;

	/**
	 * Initialize with context and the values for a new track
	 * 
	 * @param c
	 *            Context of the application
	 * @param jobID
	 *            Each track has a job related
	 */
	public DoNewTrack(Context c, long jobID) {
		super(c);

		Log.i(TAG, "Initializing " + DoNewTrack.class.getSimpleName());

		this.mJobID = jobID;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Creating track...");

		ContentValues values = new ContentValues();
		values.put(Consts.KEY_JOB_ID, mJobID);
		values.put(Consts.KEY_DISTANCE_TRAVELED, 0);
		this.mGeneratedId = this.getDatabase().insert(Consts.TABLE_TRACKS, null, values);
	}

	public long getmGeneratedId() {
		return mGeneratedId;
	}
}
