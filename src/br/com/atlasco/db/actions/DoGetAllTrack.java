package br.com.atlasco.db.actions;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.db.utils.Consts;

/**
 * Get tracks of a job
 * 
 * @author eduardo-lima
 *
 */
public class DoGetAllTrack extends ActionBase {

	private static final String TAG = DoGetAllTrack.class.getSimpleName();

	private long mJobID;

	private List<Track> mList;

	/**
	 * Assign the job id
	 * 
	 * @param c
	 *            Context of the application
	 * @param id
	 *            Job id
	 */
	public DoGetAllTrack(Context c, long id) {
		super(c);

		Log.i(TAG, "Initializing " + DoGetAllTrack.class.getSimpleName());

		mJobID = id;
		
		this.mList = new ArrayList<Track>();
	}

	@Override
	protected void execute() throws Exception {
		Log.i(TAG, "Getting tracks of job " + mJobID);

		String[] args = { Long.toString(mJobID) };
		Cursor cursor = this.getDatabase().query(Consts.TABLE_TRACKS, null, Consts.KEY_JOB_ID + " = ?", args, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Track t = new Track();
			t.setId(cursor.getLong(cursor.getColumnIndex(Consts.KEY_ID)));
			t.setJobId(cursor.getLong(cursor.getColumnIndex(Consts.KEY_JOB_ID)));
			t.setDistanceTraveled(cursor.getFloat(cursor.getColumnIndex(Consts.KEY_DISTANCE_TRAVELED)));

			this.mList.add(t);

			cursor.moveToNext();
		}
	}

	public List<Track> getmList() {
		return mList;
	}
}
