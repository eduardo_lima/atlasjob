package br.com.atlasco.db.actions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.atlasco.R;
import br.com.atlasco.db.utils.Consts;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

/**
 * Action to create new jobs
 * 
 * @author eduardo-lima
 *
 */
public class DoNewJob extends ActionBase{

	private static final String TAG = "DoNewJob";

	private final String mDescription;
	private final Context mContext;
	private long mGeneratedId;

	/**
	 * Initialize with context and the values for a new job
	 * 
	 * @param c
	 *            Context of the application
	 * @param description
	 *            Description of the task F
	 */
	public DoNewJob(Context c, String d) {
		super(c);
		
		Log.i(TAG, "Initializing DoNewJob");

		mDescription = d;
		mContext = c;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Creating job...");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

		ContentValues values = new ContentValues();
		values.put(Consts.KEY_DESCRIPTION, mDescription);
		values.put(Consts.KEY_CREATED_AT, dateFormat.format(new Date()));
		values.put(Consts.KEY_STATUS, mContext.getString(R.string.status_in_transit));
		this.mGeneratedId = this.getDatabase().insert(Consts.TABLE_JOBS, null, values);
	}

	public long getmGeneratedId() {
		return mGeneratedId;
	}
}
