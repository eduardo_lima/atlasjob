package br.com.atlasco.db.actions;

import br.com.atlasco.db.utils.Consts;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

/**
 * Action to create new coordinates
 * 
 * @author eduardo-lima
 *
 */
public class DoNewLocation extends ActionBase {
	
	private static final String TAG = DoNewLocation.class.getSimpleName();

	private final long mTrackID;
	
	private final double mLatitude;
	private final double mLongitude;
	
	/**
	 * Initialize with context and the values for a new coordinate
	 * 
	 * @param c
	 * @param trackID Track Id
	 * @param latitude Latitude
	 * @param longitude Longitude
	 */
	public DoNewLocation(Context c, long trackID, double latitude, double longitude) {
		super(c);

		Log.i(TAG, "Initializing " + DoNewLocation.class.getSimpleName());

		this.mTrackID = trackID;
		this.mLatitude = latitude;
		this.mLongitude = longitude;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Creating coordinate...");

		ContentValues values = new ContentValues();
		values.put(Consts.KEY_TRACK_ID, mTrackID);
		values.put(Consts.KEY_LATITUDE, mLatitude);
		values.put(Consts.KEY_LONGITUDE, mLongitude);
		this.getDatabase().insert(Consts.TABLE_LOCATIONS, null, values);
	}


}
