package br.com.atlasco.db.actions;

import br.com.atlasco.db.utils.DBException;
import br.com.atlasco.db.utils.DatabaseHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * This class maintains the database connection and supports adding, list task
 * and etc.
 * 
 * @author eduardo-lima
 *
 */
public abstract class ActionBase {

	private static final String TAG = "ActionBaseDB";

	// Execute SQL commands to select, insert and delete
	private SQLiteDatabase database;

	protected SQLiteDatabase getDatabase() {
		return database;
	}

	// Open/close database objects
	private DatabaseHelper dbHelper;

	/**
	 * Creates an object to help inserting, selecting and/or deleting rows
	 * 
	 * @param c
	 *            Context of the application
	 */
	public ActionBase(Context c) {
		Log.i(TAG, "Getting helper instance");

		this.dbHelper = DatabaseHelper.getInstance(c);
	}

	/**
	 * Allows the database to be used for writing or reading
	 */
	public void open() {
		Log.i(TAG, "Opening the database");

		this.database = this.dbHelper.getWritableDatabase();
	}

	/**
	 * Blocks the database interaction
	 */
	public void close() {
		Log.i(TAG, "Closing the database");

		this.dbHelper.close();
	}

	/**
	 * Sets all the rules for execute the SQL statement
	 */
	protected void validate() throws Exception {
		// do nothing, some actions do not need preparation
	}

	/**
	 * Allow SQL execution, subclasses can set this rule
	 * 
	 * @return
	 */
	protected boolean allowExecute() {
		return true;
	}

	/**
	 * Executes the SQL statement
	 */
	protected abstract void execute() throws Exception;

	/**
	 * Whole methods for database interactions
	 * 
	 * @throws DBException
	 *             On execute SQL command
	 */
	public void run() throws DBException {
		try {
			// Rules
			this.validate();

			// Open database
			this.open();
			// Allow SQL
			if (this.allowExecute()) {
				// Execute
				this.execute();
			}
			// Close database
			this.close();

		} catch (Exception e) {
			// Exception here
			throw new DBException(e.getMessage());
		}
	}
}
