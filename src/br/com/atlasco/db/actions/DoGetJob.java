package br.com.atlasco.db.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.utils.Consts;

/**
 * Get specifc job from db
 * 
 * @author eduardo-lima
 *
 */
public class DoGetJob extends ActionBase {

	private static final String TAG = "DoGetJob";

	private long mJobID;

	private Job mJob;

	/**
	 * Assign the job id
	 * 
	 * @param c
	 *            Context of the application
	 * @param id
	 *            Job id
	 */
	public DoGetJob(Context c, long id) {
		super(c);

		Log.i(TAG, "Initializing DoGetJob");

		mJobID = id;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Getting job...");

		String selectQuery = "SELECT " + Consts.TABLE_JOBS + "." + Consts.KEY_ID + " C1, " + Consts.TABLE_JOBS + "."
				+ Consts.KEY_DESCRIPTION + " C2, " + Consts.TABLE_JOBS + "." + Consts.KEY_CREATED_AT + " C3, " + Consts.TABLE_JOBS + "."
				+ Consts.KEY_STATUS + " C4  FROM " + Consts.TABLE_JOBS + " WHERE C1 = " + mJobID;

		Cursor cursor = this.getDatabase().rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			mJob = new Job();

			mJob.setId(cursor.getInt(cursor.getColumnIndex("C1")));
			mJob.setDescription(cursor.getString(cursor.getColumnIndex("C2")));
			mJob.setStatus(cursor.getString(cursor.getColumnIndex("C4")));

			try {
				mJob.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(cursor.getString(cursor
						.getColumnIndex("C3"))));
			} catch (ParseException e) {
				// silent
			}
		}
	}

	public Job getmJob() {
		return mJob;
	}
}
