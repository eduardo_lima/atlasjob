package br.com.atlasco.db.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.atlasco.db.model.Marking;
import br.com.atlasco.db.utils.Consts;

/**
 * Action to get markings of a specific job
 * 
 * @author eduardo-lima
 *
 */
public class DoGetMarking extends ActionBase {

	private static final String TAG = "DoGetMarking";

	private final long mJobID;

	private List<Marking> mList;

	public DoGetMarking(Context c, long jobID) {
		super(c);

		Log.i(TAG, "Initializing DoGetMarking");

		mJobID = jobID;
		this.mList = new ArrayList<Marking>();
	}

	@Override
	protected void execute() throws Exception {
		Log.i(TAG, "Getting markings...");

		String selectQuery = "SELECT " + Consts.TABLE_MARKINGS + "." + Consts.KEY_ID + " C1, " + Consts.TABLE_MARKINGS + "."
				+ Consts.KEY_ACTION_ID + " C2, " + Consts.TABLE_MARKINGS + "." + Consts.KEY_CREATED_AT + " C3 FROM " + Consts.TABLE_MARKINGS
				+ " WHERE " + Consts.TABLE_MARKINGS + "." + Consts.KEY_JOB_ID + " = " + mJobID;

		Cursor cursor = this.getDatabase().rawQuery(selectQuery, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Marking m = new Marking();
			m.setId(cursor.getInt(cursor.getColumnIndex("C1")));
			m.setActionId(cursor.getInt(cursor.getColumnIndex("C2")));
			m.setJobId(mJobID);
			try {
				m.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(cursor.getString(cursor
						.getColumnIndex("C3"))));
			} catch (ParseException e) {
				// silent
			}
			mList.add(m);

			cursor.moveToNext();
		}
	}

	public List<Marking> getmList() {
		return mList;
	}
}
