package br.com.atlasco.db.actions;

import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.utils.Consts;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

/**
 * Action to update jobs
 * 
 * @author eduardo-lima
 *
 */
public class DoUpdateJob extends ActionBase {

	private final String TAG = DoUpdateJob.class.getSimpleName();

	private final Job mUpdateJob;

	public DoUpdateJob(Context c, Job job) {
		super(c);

		Log.i(TAG, "Initializing " + DoUpdateJob.class.getSimpleName());

		mUpdateJob = job;
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Updating job...");

		ContentValues values = new ContentValues();
		values.put(Consts.KEY_DESCRIPTION, mUpdateJob.getDescription());
		values.put(Consts.KEY_STATUS, mUpdateJob.getStatus());
		this.getDatabase().update(Consts.TABLE_JOBS, values, Consts.KEY_ID + " = " + mUpdateJob.getId(), null);
	}

}
