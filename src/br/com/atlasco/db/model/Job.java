package br.com.atlasco.db.model;

import java.util.Date;

/**
 * Contains information of jobs
 * 
 * @author eduardo-lima
 *
 */
public class Job {

	private int id;
	private String description;
	private Date createdAt;
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
